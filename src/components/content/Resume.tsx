import * as React from 'react';

import Starfield from '~/components/layout/ui/Starfield';

const ResumeBody = () => {
    return (
        <div id="resume-body">
            <section>
                <h1>Personal</h1>
                <aside>
                    <aside className="pretty">Name: </aside>
                    <aside>Marek Małecki</aside>
                </aside>
                <aside>
                    <aside className="pretty">Birthdate: </aside>
                    <aside>19-03-2000</aside>
                </aside>
                <aside>
                    <aside className="pretty">Email: </aside>
                    <a href="mailto:marekczjk@gmail.com">
                        <aside>marekczjk@gmail.com</aside>
                    </a>
                </aside>
            </section>
            <div className="separator" />
            <section>
                <h1>Profile</h1>
                <div>
                    I'm an IT graduate coming from a vocational school in Poland and I wish to acquire professional experience
                    in the field. <br />I specialize in hardware and software repairs, frontend web development and remote tech
                    support. <br />
                    <br />
                    My hobbies include programming, music production, remote repairs, PC building and playing around with
                    obscure operating systems.
                </div>
            </section>
            <div className="separator" />
            <section>
                <h1>Education and Achievements</h1>
                <div>
                    <div className="list">
                        <div className="list-header">
                            <span>28-03-2013</span> - Szkoła Podstawowa Nr. 17 w Żorach:
                        </div>
                        <div className="list-item">
                            <div>3rd place in "FOX" &ndash; poviat English language competition.</div>
                        </div>
                        <div className="list-header">
                            <span>28-03-2016</span> - Gimnazjum Nr. 4 w Żorach:
                        </div>
                        <div className="list-item">
                            <div>Participation in the CyberFair Internet 2014 project.</div>
                            <div>Participation in the Żory &ndash; Town of Artists 2014 project.</div>
                            <div>ECDL Base certificate.</div>
                        </div>
                        <div className="list-header">
                            <span>28-04-2020</span> - Technikum Nr.1 w Żorach:
                        </div>
                        <div className="list-item">
                            <div>Participation in the "Let's Code 2016" programming competition.</div>
                            <div>3rd place in the IV interschool "Day with Passion" organized by ZST Rybnik.</div>
                        </div>
                    </div>
                </div>
            </section>
            <div className="separator" />
            <section>
                <h1>Communication skills</h1>
                <aside>
                    <aside className="pretty">Polish: </aside>
                    <aside>Native, proficient in grammar</aside>
                </aside>
                <aside>
                    <aside className="pretty">English: </aside>
                    <aside>Very fluent (written and spoken)</aside>
                </aside>
            </section>
            <div className="separator" />
            <section>
                <h1>Technology summary</h1>
                <div>
                    <div className="list">
                        <div className="list-header">
                            <span>Programming Languages:</span>
                        </div>
                        <div className="list-item">
                            <div>C, C#, C++, Typescript, Javascript, JSX, Python, Rust</div>
                        </div>
                        <div className="list-header">
                            <span>Web Frameworks:</span>
                        </div>
                        <div className="list-item">
                            <div>React, Node, SASS/SCSS</div>
                        </div>
                        <div className="list-header">
                            <span>Source Code Control and Tools:</span>
                        </div>
                        <div className="list-item">
                            <div>Git, SSH, Oracle's VM VirtualBox</div>
                        </div>
                        <div className="list-header">
                            <span>IDEs and Editors:</span>
                        </div>
                        <div className="list-item">
                            <div>Visual Studio, Visual Studio Code, Vim, Joe's Own Editor, IntelliJ IDEA, Pycharm</div>
                        </div>
                        <div className="list-header">
                            <span>Operating Systems:</span>
                        </div>
                        <div className="list-item">
                            <div>Microsoft (DOS to Win 10), Linux flavors, BSD, HaikuOS, ReactOS</div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    );
};

const Resume = () => (
    <article id="resume">
        <Starfield>
            <div id="content">
                <ResumeBody />
            </div>
        </Starfield>
    </article>
);

export default Resume;
