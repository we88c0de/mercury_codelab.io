import * as React from 'react';

import Banner from '~/components/layout/ui/Banner';
import Starfield from '~/components/layout/ui/Starfield';

const Homepage = () => (
    <article id="homepage">
        <Starfield>
            <div id="content">
                <Banner />
                <h1>Marek Małecki</h1>
                <h2>Frontend Developer & Linux Enthusiast</h2>
            </div>
        </Starfield>
    </article>
);

export default Homepage;
