import * as React from 'react';

const svg = require('~/../assets/graphics/Saturn.svg');

const Banner = () => (
    <div id="banner">
        <img src={svg}></img>
    </div>
);

export default Banner;
