import * as React from 'react';

const Loader = () => (
    <div id="loader-wrapper">
        <div className="loader">
            <div className="loader-section"></div>
            <div className="loader-section delayed"></div>
        </div>
    </div>
);

export default Loader;
