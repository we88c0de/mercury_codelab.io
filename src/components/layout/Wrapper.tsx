import * as React from 'react';

import Display from '~/components/layout/Display';
import Loader from '~/components/layout/Loader';
import Navbar from '~/components/layout/ui/Navbar';

type WrapperProps = {
    children?: React.ReactNode;
};

const Wrapper = (props: WrapperProps) => (
    <div id="wrapper">
        <Navbar />
        <Display>{props.children}</Display>
        <Loader />
    </div>
);

export default Wrapper;
