<img src="meta/Saturn.svg" width="128" height="128">

# Mercury's Main Page

This is a repo for my personal webpage hosting.

## Private Homepage

This repo _will_ include my private homepage which I use across various browsers and devices.

## Portfolio

My entire portfolio is there too...

## Resume

...coupled with the CV.

## Built with

React, SASS, Node, Typescript

## License

Refer to [LICENSE]('https://gitlab.com/mercury_code/mercury_code.gitlab.io/-/blob/master/LICENSE') file.
